package br.com.mastertech.msporta;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
public class MsportaApplication {

	public static void main(String[] args) {
		SpringApplication.run(MsportaApplication.class, args);
	}

}
