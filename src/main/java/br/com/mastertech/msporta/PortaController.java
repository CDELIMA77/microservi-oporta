package br.com.mastertech.msporta;

import org.hibernate.ObjectNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import java.util.Optional;

@RestController
@RequestMapping("/porta")
public class PortaController {

    @Autowired
    private PortaServices portaServices;

    @GetMapping("/{id}")
    public ResponseEntity<Optional<Porta>> buscarPorta(@PathVariable Long id){
        Optional<Porta> portaOptional = portaServices.buscarPorId(id);
        if (portaOptional.isPresent()){
            return ResponseEntity.status(200).body(portaOptional);
        } else {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping
    public ResponseEntity<Porta> incluirPorta(@RequestBody Porta porta) {
        try { portaServices.salvarPorta(porta); }
        catch (ObjectNotFoundException e){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
        return ResponseEntity.status(201).body(porta);
    }
}
