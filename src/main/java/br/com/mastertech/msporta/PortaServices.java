package br.com.mastertech.msporta;

import org.hibernate.ObjectNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.Optional;

@Service
public class PortaServices {

    @Autowired
    private PortaRepository portaRepository;

    public Optional<Porta> buscarPorId(Long id) {
        Optional<Porta> portaOptional = portaRepository.findById(id);
        return portaOptional;
    }

    public Porta salvarPorta(Porta porta) throws ObjectNotFoundException {
        portaRepository.save(porta);
        return porta;
        }
}
